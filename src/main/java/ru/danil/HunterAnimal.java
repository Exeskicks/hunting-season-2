package ru.danil;

public class HunterAnimal {
    Animal currentAnimal;


    public void setCurrentAnimal(Animal currentAnimal){
        this.currentAnimal = currentAnimal;
    }

    public void killAnimal(){
        System.out.println("Cтреляю в " + currentAnimal.name);
        System.out.println("Попал");
        System.out.println(currentAnimal.sayBeforeDeath());
    }
}
