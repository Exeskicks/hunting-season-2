package ru.danil;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HunterSeason {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
            new ClassPathXmlApplicationContext("applicationContext.xml");


        HunterAnimal hunterAnimalGoat = context.getBean("hunterGoat", HunterAnimal.class);
        hunterAnimalGoat.killAnimal();

        HunterAnimal hunterAnimalFrog = context.getBean("hunterFrog", HunterAnimal.class);
        hunterAnimalFrog.killAnimal();

        HunterAnimal hunterAnimalGoose = context.getBean("hunterGoose", HunterAnimal.class);
        hunterAnimalGoose.killAnimal();
        context.close();
    }
}
