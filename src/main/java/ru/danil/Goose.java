package ru.danil;

public class Goose extends Animal{

    Goose(String name){
        this.name = name;
    }
    @Override
    String sayBeforeDeath() {
        return "Кря кря!";
    }
}
