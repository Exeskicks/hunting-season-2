package ru.danil;

public class Frog extends Animal{

    Frog(String name){
        this.name = name;
    }

    @Override
    String sayBeforeDeath() {
        return "Ква ква!";
    }
}
